import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { Post as PostEntity } from 'src/Post';
import { PostService } from './post.service';

@Controller('post')
export class PostController {

    constructor(private readonly postService: PostService) {}

  @Post('/addpost') // POST /albums
  @HttpCode(HttpStatus.CREATED)
  async createAlbum(@Body() body:PostEntity){
    return await this.postService.createOrUpdate(body);
  }
  
}
